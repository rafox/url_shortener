<div class="siimple-table-body">
        <div class="siimple-table-row">
            <div class="siimple-table-cell"> {{ $link->url }}</div>
            <div class="siimple-table-cell"> {{ date('d-m-Y', strtotime($link->created_at)) }}</div>
            <div class="siimple-table-cell">{{ $link->id }}</div>
            <div class="siimple-table-cell"><i class="fas fa-pencil"></i> - <i class="fas fa-trash"></i></div>
        </div>
    </div>