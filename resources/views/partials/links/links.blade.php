<div class="siimple-table siimple-table--hover">
        <div class="siimple-table-header">
            <div class="siimple-table-row">
                <div class="siimple-table-cell">URL d'origine</div>
                <div class="siimple-table-cell">Créé le</div>
                <div class="siimple-table-cell">URL courte</div>
                <div class="siimple-table-cell"></div>
            </div>
        </div>
        @foreach ($links as $link)
        @include('partials.links.link', ['link' => $link])
        @endforeach
    </div>
    