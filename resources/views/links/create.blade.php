@extends('layouts.app')

@section('content')

<div class="siimple-form">
        <form action="" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="siimple-form-title">Le Super UrlShortener</div>
            <div class="siimple-form-detail">C'est le moment de simplifier vos liens.</div>
            <div class="siimple-form-field">
                <input type="text" class="siimple-input siimple-input--fluid" name="url" placeholder="Votre URL d'origine ici">
            </div>
            <div class="siimple-form-field">
                <button class="siimple-btn siimple-btn--blue">Raccourcir l'URL</button>
            </div>
        </form>
        @include('partials.links.links')
        <div class="siimple-btn siimple-btn--navy"><i class="fas fa-plus-square"></i> Plus</div>
</div>

   
@endsection