<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Link;

class LinksController extends Controller
{
    //Retourne la view de creation d'une short_url
    public function create() {

        $links = Link::all();
        return view('links.create', compact('links'));

    }

    public function store(Request $request) {
        $url = $request->input('url');
        $link = Link::firstOrCreate(['url' => $url]);
    }
}
